import com.devcamp.jbr250.Account;
import com.devcamp.jbr250.Customer;

public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer(1, "Duy", 10);
        Customer customer2 = new Customer(2, "Huong", 20);

        //Subtask 4
        System.out.println(customer1.toString());
        System.out.println(customer2.toString());

        //Subtask 5
        Account account1 = new Account(10, customer1, 100000);
        Account account2 = new Account(20, customer2, 200000);

        System.out.println(account1.toString());
        System.out.println(account2.toString());
    }
}
