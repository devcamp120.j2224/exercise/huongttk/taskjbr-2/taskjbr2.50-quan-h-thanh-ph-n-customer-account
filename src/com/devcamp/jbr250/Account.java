package com.devcamp.jbr250;

public class Account {
    private int id = 0;
    Customer customer;
    private double balance;
    
    public Account(int id, Customer customer, double balance) {
        this.id = id;
        this.customer = customer;
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
    public String getCustomerName(){
        return customer.getName();
    }
    public double deposit(double amount){
        return this.balance = balance + amount;
    }
    public double withdraw(double amount){
        if(balance > amount){
            this.balance = balance - amount;
            
        } else {
            System.out.println("Amount withdraw exceeds the current balance");
        }
        return balance;
    }

    @Override
    public String toString() {
        return customer.getName() + " (" + id + ")" + " balance= " + "$" + String.format("%.2f", balance);
    }
}
